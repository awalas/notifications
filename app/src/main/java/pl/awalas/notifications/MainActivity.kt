package pl.awalas.notifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.RemoteInput
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var pendingIntent: PendingIntent


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createNotificationChannel()
        createBasicNotification()
        createExpandableNotification()
        createReplyNotification()
        createSummaryNotification()
    }

    private fun createBasicNotification() {
        val runActivityIntent = Intent(this, AlertActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        pendingIntent = PendingIntent.getActivity(this, 0, runActivityIntent, 0)

        val basicNotificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.abc_ic_star_black_48dp)
            .setContentTitle("My notification")
            .setContentText("Much longer text that cannot fit one line...")
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setGroup(GROUP_KEY_WORK_EMAIL)

        basic_notification_button.setOnClickListener {
            with(NotificationManagerCompat.from(this)) {
                // notificationId is a unique int for each notification that you must define
                notify(1, basicNotificationBuilder.build())
            }
        }
    }

    private fun createExpandableNotification() {
        val runActivityIntent = Intent(this, AlertActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        pendingIntent = PendingIntent.getActivity(this, 0, runActivityIntent, 0)

        val basicNotificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.abc_ic_star_black_48dp)
            .setContentTitle("My notification")
            .setContentText("Much longer text that cannot fit one line...")
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.kot1))
            .setStyle(NotificationCompat.BigPictureStyle()
                .bigPicture(BitmapFactory.decodeResource(resources, R.drawable.kot2))
                .bigLargeIcon(null))
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setGroup(GROUP_KEY_WORK_EMAIL)

        expandable_notification_button.setOnClickListener {
            with(NotificationManagerCompat.from(this)) {
                // notificationId is a unique int for each notification that you must define
                notify(2, basicNotificationBuilder.build())
            }
        }
    }

    private fun createReplyNotification() {
        val runActivityIntent = Intent(this, AlertActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        pendingIntent = PendingIntent.getActivity(this, 0, runActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val remoteInput: RemoteInput = RemoteInput.Builder(KEY_TEXT_REPLY).run {
            setLabel("REPLY")
            build()
        }

        val action: NotificationCompat.Action =
            NotificationCompat.Action.Builder(R.drawable.abc_ic_star_half_black_48dp, "LABEL", pendingIntent)
                .addRemoteInput(remoteInput)
                .build()

        val newMessageNotification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.abc_ic_star_black_48dp)
            .setContentTitle("My notification")
            .setContentText("Much longer text that cannot fit one line...")
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(
                        "Much longer text that cannot fit one line..." +
                                "Much longer text that cannot fit one line..." +
                                "Much longer text that cannot fit one line..." +
                                "Much longer text that cannot fit one line..."
                    )
            )
            .addAction(action)
            .setGroup(GROUP_KEY_WORK_EMAIL)

        reply_notification_button.setOnClickListener {
            with(NotificationManagerCompat.from(this)) {
                notify(3, newMessageNotification.build())
            }
        }
    }

    private fun createSummaryNotification() {
        val basicNotificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.abc_ic_star_black_48dp)
            .setStyle(NotificationCompat.InboxStyle())
            .setGroupSummary(true)
            .setGroup(GROUP_KEY_WORK_EMAIL)

        summary_notification_button.setOnClickListener {
            with(NotificationManagerCompat.from(this)) {
                // notificationId is a unique int for each notification that you must define
                notify(4, basicNotificationBuilder.build())
            }
        }
    }

    private fun createNotificationChannel() {
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
        // Register the channel with the system
        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
    }

    companion object {
        const val KEY_TEXT_REPLY = "key_text_reply"
        const val CHANNEL_ID = "channel_id"
        const val CHANNEL_NAME = "Notification Channel"
        const val GROUP_KEY_WORK_EMAIL = "GRUPA_KOTOW"
    }

}
