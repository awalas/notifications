package pl.awalas.notifications

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.RemoteInput
import kotlinx.android.synthetic.main.activity_alert.*
import pl.awalas.notifications.MainActivity.Companion.KEY_TEXT_REPLY

class AlertActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alert)

        processInlineReply(intent)
    }

    private fun processInlineReply(intent: Intent) {
        val remoteInput = RemoteInput.getResultsFromIntent(intent)

        if (remoteInput != null) {
            val reply = remoteInput.getCharSequence(KEY_TEXT_REPLY)!!.toString()

            //Set the inline reply text in the TextView
            reply_text.text = "Reply is $reply"

            val newMessageNotification = NotificationCompat.Builder(this, MainActivity.CHANNEL_ID)
                .setSmallIcon(R.drawable.abc_ic_star_black_48dp)
                .setContentTitle("Inline Reply received")

            with(NotificationManagerCompat.from(this)) {
                notify(3, newMessageNotification.build())
            }
        }
    }
}
